# one_time_pad.py
Encrypts and decrypts messages using the one time pad cryptographic method.

## Usage, from start to finish

When first executing the program, you'll be prompted to either:
1. Have messages printed to the console and inserted through the console
2. Have messages read through text files and saved as text files

You'll then be asked to either encrypt or decrypt a message.

### Console Mode

#### Encrypting Messages

1. Enter a message for encryption.
2. If you'd like to generate new shifts, or if this is the first time you've used the program, choose to generate new shifts.
3. Your new message will be displayed.

#### Decrypting Messages

1. Enter an encrypted message, making sure that the shifts file that was used to encrypt the message is present.
2. Your unencrypted message will be displayed.

### File Interaction Mode

#### Encrypting Messages
1. Before running the program, make sure that your message is in the message.txt file.
2. Choose to encrypt at the prompt.
3. Your encrypted message will overwrite the unencrypted message in the message.txt file.

#### Decrypting Messages
1. Before running the program, make sure that your encrypted message is in the message.txt file.
2. Choose to decrypt at the prompt.
3. Your unencrypted message will overwrite the encrypted message in the message.txt file.
